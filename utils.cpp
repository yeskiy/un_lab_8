#include "utils.h"

void clearStreamInput() {
    std::cin.clear();
    std::string ignoreLine;
    getline(std::cin, ignoreLine);
}

int requireInt(std::string requiredText) {
    bool isTypedRight = false;
    int result = 0;
    while (!isTypedRight) {
        std::cout << requiredText;
        try {
            std::cin >> result;
            std::cin.exceptions(std::istream::failbit | std::istream::badbit);
            std::cout << std::endl;
            isTypedRight = true;
        } catch (...) {
            clearStreamInput();
            std::cout << "Please, enter only numbers!" << std::endl;
        }
        clearStreamInput();
    }
    return result;
};

int findMin(int i1, int i2) {
    if (i1 < i2) {
        return i1;
    } else {
        return i2;
    }
}

int findMax(int i1, int i2) {
    if (i1 > i2) {
        return i1;
    } else {
        return i2;
    }
}

bool exitFromCircle(std::string leaveText) {
    char input;
    printf(leaveText.c_str());
    scanf(" %c", &input);
    clearStreamInput();
    if (input == 'q') {
        return true;
    }
    std::cout << std::endl;
    return false;
};

int taskUAChooser(int limit) {

    bool isTypedRight = false;
    int result = 0;
    while (!isTypedRight) {
        std::cout << "Choose task (1 - " << limit << ")" << std::endl;
        try {
            std::cin.exceptions(std::istream::failbit | std::istream::badbit);
            std::cin >> result;
            if (result > limit || result < 1) {
                throw RuntimeException("received a bad int");
            }
            isTypedRight = true;
        } catch (...) {
            clearStreamInput();
            std::cout << "Please, enter only numbers (1 - " << limit << ")" << std::endl;
        }
        clearStreamInput();
    }
    return result;

}
std::string generateLine(int length) {
    std::string line;
    for (int i = 0; i < length; i++) {
        line += "#";
    }
    return line;
}