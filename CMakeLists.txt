cmake_minimum_required(VERSION 3.20)
project(Un_lab_8)

set(CMAKE_CXX_STANDARD 14)

add_executable(Un_lab_8 main.cpp utils.cpp utils.h example1.cpp example1.h example2.cpp example2.h tasks.cpp tasks.h)
