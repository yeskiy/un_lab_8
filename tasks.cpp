#include "tasks.h"
#include "ctime"
#include "cmath"

void firstTask() {
    time_t currentTime = time(0);

    struct tm *tm = localtime(&currentTime);
    tm->tm_mday += 1;
    time_t newTime = mktime(tm);

    char buffer[32];
    std::tm *ptm = localtime(&newTime);
    std::strftime(buffer, 32, "%d-%m-%Y", ptm);

    printWithSpace("The next day is", buffer);
}

void secondTask() {
    const int x = requireInt("Type x: ");
    const int y = requireInt("Type y: ");
    const int z = requireInt("Type z: ");
    const double denominator = pow(x, z) + z;
    if (denominator == 0) throw RuntimeException("Denominator equals to Zero. Bad Arguments");
    printWithSpace("result:", (std::min(z, std::max(x, y))) / denominator);
}