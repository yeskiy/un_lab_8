#include "example2.h"
#include "iostream"

int example2() {
    int a, b, max;

    std::cout << "Input a, b:";
    std::cin >> a >> b;

    if (b >= a) max = b;
    else max = a;

    std::cout << "max = " << max << std::endl;

    return 0;
}