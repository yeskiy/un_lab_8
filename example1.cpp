#include "example1.h"
#include "iostream"

int example1() {
    int a, b, max;

    std::cout << "Input a, b:";    //запрошення до введення
    std::cin >> a >> b;            //введення даних з клавіатури

    max = a;
    if (b > max) max = b;

    std::cout << "max = " << max << std::endl;

    return 0;
}