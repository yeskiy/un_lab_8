#ifndef UN_LAB_8_UTILS_H
#define UN_LAB_8_UTILS_H

#include <string>
#include "iostream"


int requireInt(std::string requiredText);

bool exitFromCircle(std::string leaveText);

void clearStreamInput();

int findMin(int i1, int i2);

int findMax(int i1, int i2);

template<typename T>
void print(T t) {
    std::cout << t << std::endl;
}

template<typename T, typename... Args>
void print(T t, Args... args) {
    std::cout << t << "  ";
    print(args...);
}

template<typename T>
void printWithSpace(T t) {
    std::cout << t << std::endl;
}

template<typename T, typename... Args>
void printWithSpace(T t, Args... args) {
    std::cout << t << " ";
    printWithSpace(args...);
}

int taskUAChooser(int limit);

std::string generateLine(int length);

class RuntimeException : public std::exception {
    std::string M;
public:
    RuntimeException(const char *errorMessage) : M(errorMessage) {}

    const char *what() const noexcept override {
        return M.c_str();
    }
};

#endif //UN_LAB_8_UTILS_H
